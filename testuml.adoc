## This works 

[plantuml, format="png", width="500px"]
----
include::uml/import.uml[]
----

## This doesn't

[plantuml, format="png", width="500px"]
----
include::uml/test.uml[]
----
